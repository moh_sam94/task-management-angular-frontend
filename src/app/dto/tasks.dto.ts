export interface createUpdateTaskRequest {
  title: string;
  description: string;
}
