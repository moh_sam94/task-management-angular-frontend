import { Location } from '@angular/common';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from 'src/app/app-routing.module';
import { NgPrimeModule } from '../shared/ng-prime.module';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let fixture: ComponentFixture<HeaderComponent>;
  let component: HeaderComponent;
  let location: Location;
  let router: Router;

  beforeEach(async () => {
    localStorage.setItem('token', JSON.stringify('some token'));
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), NgPrimeModule],
      declarations: [HeaderComponent],
    }).compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    router.navigate(['/tasks']);
    fixture.detectChanges();
  });

  afterEach(() => {
    localStorage.removeItem('token');
  });

  it('should navigate to create task page when clicking createTask button', fakeAsync(() => {
    expect(location.path()).toBe('/tasks');

    const createTaskButton = fixture.debugElement.query(
      By.css('#header-create-task-button')
    );
    createTaskButton.triggerEventHandler('click', null);
    tick();
    expect(location.path()).toBe('/tasks/create');
  }));

  it('should logout and remove token from token from local storage', fakeAsync(() => {
    expect(location.path()).toBe('/tasks');
    const logoutButton = fixture.debugElement.query(
      By.css('#header-logout-button')
    );
    logoutButton.triggerEventHandler('click', null);
    tick();
    expect(location.path()).toBe('/login');
    expect(localStorage.getItem('token')).toBe(null);
  }));
});
