import { NgModule } from '@angular/core';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { MenubarModule } from 'primeng/menubar';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  imports: [
    InputTextModule,
    ButtonModule,
    TableModule,
    MenubarModule,
    DropdownModule,
  ],
  providers: [],
  exports: [
    InputTextModule,
    ButtonModule,
    TableModule,
    MenubarModule,
    DropdownModule,
  ],
})
export class NgPrimeModule {}
