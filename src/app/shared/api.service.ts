import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../tasks/task.model';
import { environment } from 'src/environments/environment';
import { User } from '../users/user.model';
import { createUpdateTaskRequest } from '../dto/tasks.dto';
import { AuthResponse } from '../dto/auth-response-dto';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  httpOptions() {
    const accessToken = JSON.parse(localStorage.getItem('token') as string);
    return {
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    };
  }

  public getTasks() {
    return this.httpClient.get<Task[]>(
      `${environment.API_SERVER}/tasks`,
      this.httpOptions()
    );
  }

  public createTask(task: createUpdateTaskRequest) {
    return this.httpClient.post<Task>(
      `${environment.API_SERVER}/tasks`,
      task,
      this.httpOptions()
    );
  }

  public updateTask(id: number, task: createUpdateTaskRequest) {
    return this.httpClient.put<Task>(
      `${environment.API_SERVER}/tasks/${id}`,
      task,
      this.httpOptions()
    );
  }

  public updateTaskStatus(id: number, status: string) {
    return this.httpClient.patch<Task>(
      `${environment.API_SERVER}/tasks/${id}/status`,
      { status },
      this.httpOptions()
    );
  }

  public deleteTask(id: number) {
    return this.httpClient.delete(
      `${environment.API_SERVER}/tasks/${id}`,
      this.httpOptions()
    );
  }

  public signIn(user: User) {
    return this.httpClient.post<AuthResponse>(
      `${environment.API_SERVER}/auth/signin`,
      user
    );
  }

  public signUp(user: User) {
    return this.httpClient.post<AuthResponse>(
      `${environment.API_SERVER}/auth/signup`,
      user
    );
  }
}
