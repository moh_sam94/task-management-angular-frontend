import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { of, Subscription, throwError } from 'rxjs';
import { ApiService } from '../shared/api.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(private apiService: ApiService, private router: Router) {}
  username: string = '';
  password: string = '';
  error: string = '';
  loginSub: Subscription = new Subscription();
  signUpSub: Subscription = new Subscription();
  ngOnInit(): void {
    const accessToken = JSON.parse(localStorage.getItem('token') as string);
    if (accessToken !== null) {
      this.router.navigate(['tasks']);
    }
  }

  login() {
    this.loginSub = this.apiService
      .signIn({
        username: this.username,
        password: this.password,
      })
      .subscribe(({ accessToken }) => {
        localStorage.setItem('token', JSON.stringify(accessToken));
        this.router.navigate(['tasks']);
      });
  }

  onSignup() {
    this.signUpSub = this.apiService
      .signUp({
        username: this.username,
        password: this.password,
      })
      .pipe(
        catchError((errorResp) => {
          if (errorResp.status === 409) {
            this.error = 'User already exists';
          } else {
            this.error = errorResp.statusText;
          }
          return throwError(errorResp);
        })
      )
      .subscribe((response) => {
        if (!response) {
          this.login();
        }
      });
  }

  ngOnDestroy() {
    this.loginSub.unsubscribe();
    this.signUpSub.unsubscribe();
  }
}
