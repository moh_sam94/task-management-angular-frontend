import { Location } from '@angular/common';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of, throwError } from 'rxjs';
import { routes } from 'src/app/app-routing.module';
import { ApiService } from 'src/app/shared/api.service';
import { AuthResponse } from '../dto/auth-response-dto';
import { NgPrimeModule } from '../shared/ng-prime.module';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let fixture: ComponentFixture<LoginComponent>;
  let component: LoginComponent;
  let signInSpy: Observable<AuthResponse>;
  let signUpSpy: Observable<any>;
  let location: Location;
  let router: Router;

  beforeEach(async () => {
    localStorage.removeItem('token');
    const apiServiceStub = jasmine.createSpyObj('ApiService', [
      'signIn',
      'signUp',
    ]);
    signInSpy = apiServiceStub.signIn.and.returnValue(
      of({ accessToken: 'some token' })
    );
    signUpSpy = apiServiceStub.signUp.and.returnValue(of(''));

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        NgPrimeModule,
      ],
      declarations: [LoginComponent],
      providers: [{ provide: ApiService, useValue: apiServiceStub }],
    }).compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    router.navigate(['login']);
  });

  afterAll(() => {
    localStorage.removeItem('token');
  });

  it('should call login with the form data and navigate to tasks', fakeAsync(() => {
    expect(location.path()).toBe('/login');

    const hostElement = fixture.nativeElement;
    const usernameInput: HTMLInputElement = hostElement.querySelector(
      '#username'
    );
    const passwordInput: HTMLInputElement = hostElement.querySelector(
      '#password'
    );

    fixture.detectChanges();
    usernameInput.value = 'user';
    passwordInput.value = 'Password12345';
    usernameInput.dispatchEvent(new Event('input'));
    passwordInput.dispatchEvent(new Event('input'));

    expect(component.username).toBe('user');
    expect(component.password).toBe('Password12345');

    const loginButton = fixture.debugElement.query(By.css('#login-button'));
    loginButton.triggerEventHandler('click', null);

    expect(signInSpy).toHaveBeenCalledWith({
      username: 'user',
      password: 'Password12345',
    });
    tick();
    expect(location.path()).toBe('/tasks');
  }));

  it('should call login() with the form data and navigate to tasks', fakeAsync(() => {
    expect(location.path()).toBe('/login');

    const hostElement = fixture.nativeElement;
    const usernameInput: HTMLInputElement = hostElement.querySelector(
      '#username'
    );
    const passwordInput: HTMLInputElement = hostElement.querySelector(
      '#password'
    );

    fixture.detectChanges();
    usernameInput.value = 'user';
    passwordInput.value = 'Password12345';
    usernameInput.dispatchEvent(new Event('input'));
    passwordInput.dispatchEvent(new Event('input'));

    expect(component.username).toBe('user');
    expect(component.password).toBe('Password12345');

    const loginButton = fixture.debugElement.query(By.css('#login-button'));
    loginButton.triggerEventHandler('click', null);

    expect(signInSpy).toHaveBeenCalledWith({
      username: 'user',
      password: 'Password12345',
    });
    tick();
    expect(location.path()).toBe('/tasks');
  }));

  it('should call onSignUp() with the form data and navigate to tasks after successfull registeration', fakeAsync(() => {
    expect(location.path()).toBe('/login');

    const hostElement = fixture.nativeElement;
    const usernameInput: HTMLInputElement = hostElement.querySelector(
      '#username'
    );
    const passwordInput: HTMLInputElement = hostElement.querySelector(
      '#password'
    );

    fixture.detectChanges();
    usernameInput.value = 'user';
    passwordInput.value = 'Password12345';
    usernameInput.dispatchEvent(new Event('input'));
    passwordInput.dispatchEvent(new Event('input'));

    expect(component.username).toBe('user');
    expect(component.password).toBe('Password12345');

    const signupButton = fixture.debugElement.query(By.css('#signup-button'));
    signupButton.triggerEventHandler('click', null);

    expect(signUpSpy).toHaveBeenCalledWith({
      username: 'user',
      password: 'Password12345',
    });
    tick();
    expect(location.path()).toBe('/tasks');
  }));
});
