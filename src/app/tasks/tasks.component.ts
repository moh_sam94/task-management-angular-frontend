import { Component, OnDestroy, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import { ApiService } from '../shared/api.service';
import { Task, TaskStatus } from './task.model';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit, OnDestroy {
  constructor(private apiService: ApiService) {}

  getTasksSub: Subscription = new Subscription();

  tasks: Task[] = [];
  editedTask: Task = <Task>{};

  readonly taskStatusList: SelectItem[] = [
    { label: TaskStatus.OPEN, value: TaskStatus.OPEN },
    { label: TaskStatus.IN_PROGRESS, value: TaskStatus.IN_PROGRESS },
    { label: TaskStatus.DONE, value: TaskStatus.DONE },
  ];

  ngOnInit(): void {
    this.loadTasks();
  }

  loadTasks() {
    this.getTasksSub = this.apiService.getTasks().subscribe((tasks) => {
      this.tasks = tasks;
    });
  }

  onRowEditInit(task: Task) {
    this.editedTask = { ...task };
  }

  onRowEditSave(task: Task) {
    if (this.editedTask.status !== task.status) {
      this.apiService.updateTaskStatus(task.id, task.status).toPromise();
    }
    if (
      task.title !== this.editedTask.title ||
      task.description !== this.editedTask.description
    )
      this.apiService.updateTask(task.id, task).toPromise();
  }

  onRowEditCancel(task: Task) {
    this.tasks = this.tasks.map((v) =>
      v.id === task.id ? this.editedTask : v
    );
  }

  onRowDelete(taskId: number) {
    this.tasks = this.tasks.filter((task) => task.id !== taskId);
    this.apiService.deleteTask(taskId).toPromise();
  }

  ngOnDestroy() {
    this.getTasksSub.unsubscribe();
  }
}
