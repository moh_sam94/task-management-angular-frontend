import { Location } from '@angular/common';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { routes } from 'src/app/app-routing.module';
import { ApiService } from 'src/app/shared/api.service';
import { Task } from '../task.model';
import { CreateTaskComponent } from './create-task.component';

describe('CreateTaskComponent', () => {
  let fixture: ComponentFixture<CreateTaskComponent>;
  let component: CreateTaskComponent;
  let createTaskSpy: Observable<Task>;
  let location: Location;
  let router: Router;
  beforeEach(async () => {
    localStorage.setItem('token', JSON.stringify('some token'));
    const apiServiceStub = jasmine.createSpyObj('ApiService', ['createTask']);
    createTaskSpy = apiServiceStub.createTask.and.returnValue(of(''));
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), FormsModule],
      declarations: [CreateTaskComponent],
      providers: [{ provide: ApiService, useValue: apiServiceStub }],
    }).compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(CreateTaskComponent);
    component = fixture.componentInstance;
    router.navigate(['tasks/create']);
  });

  afterEach(() => {
    localStorage.removeItem('token');
  });
  it('should not create a task and navigate to tasks when pressing cancel', fakeAsync(() => {
    expect(location.path()).toBe('/tasks/create');
    const cancelButton = fixture.debugElement.query(
      By.css('#create-task-cancel-button')
    );
    cancelButton.triggerEventHandler('click', null);
    tick();
    expect(location.path()).toBe('/tasks');
  }));

  it('should call onCancel() when Clicking the cancel button', fakeAsync(() => {
    spyOn(component, 'onCancel');
    const cancelButton = fixture.debugElement.query(
      By.css('#create-task-cancel-button')
    );
    cancelButton.triggerEventHandler('click', null);
    expect(component.onCancel).toHaveBeenCalled();
  }));

  it('should call onSubmit when clicking create task button', fakeAsync(() => {
    spyOn(component, 'onSubmit');
    const createButton = fixture.debugElement.query(
      By.css('#create-task-confirm-button')
    );
    createButton.triggerEventHandler('click', null);
    tick();
    expect(component.onSubmit).toHaveBeenCalled();
  }));

  it('should call createTask with the form data and navigate to tasks', fakeAsync(() => {
    expect(location.path()).toBe('/tasks/create');

    const hostElement = fixture.nativeElement;
    const titleInput: HTMLInputElement = hostElement.querySelector('#title');
    const descriptionInput: HTMLInputElement = hostElement.querySelector(
      '#description'
    );

    fixture.detectChanges();
    titleInput.value = 'some title';
    descriptionInput.value = 'hi';
    titleInput.dispatchEvent(new Event('input'));
    descriptionInput.dispatchEvent(new Event('input'));

    expect(component.title).toBe('some title');
    expect(component.description).toBe('hi');

    const createButton = fixture.debugElement.query(
      By.css('#create-task-confirm-button')
    );
    createButton.triggerEventHandler('click', null);

    expect(createTaskSpy).toHaveBeenCalledWith({
      title: 'some title',
      description: 'hi',
    });
    tick();
    expect(location.path()).toBe('/tasks');
  }));
});
