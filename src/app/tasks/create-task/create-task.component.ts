import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tasks',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss'],
})
export class CreateTaskComponent implements OnInit {
  constructor(private apiService: ApiService, private router: Router) {}
  title: string = '';
  description: string = '';
  ngOnInit(): void {}

  onSubmit() {
    this.apiService
      .createTask({ title: this.title, description: this.description })
      .toPromise()
      .then(() => {
        this.router.navigate(['tasks']);
      });
  }

  onCancel() {
    this.router.navigate(['tasks']);
  }
}
