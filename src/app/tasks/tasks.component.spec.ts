import { Location } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Dropdown } from 'primeng/dropdown';
import { Observable, of } from 'rxjs';
import { routes } from 'src/app/app-routing.module';
import { ApiService } from 'src/app/shared/api.service';
import { HeaderComponent } from '../header/header.component';
import { NgPrimeModule } from '../shared/ng-prime.module';
import { Task, TaskStatus } from './task.model';
import { TasksComponent } from './tasks.component';

describe('TasksComponent', () => {
  let fixture: ComponentFixture<TasksComponent>;
  let component: TasksComponent;
  let getTasksSpy: Observable<Task[]>;
  let location: Location;
  let router: Router;
  beforeEach(async () => {
    localStorage.setItem('token', JSON.stringify('some token'));
    const apiServiceStub = jasmine.createSpyObj('ApiService', [
      'getTasks',
      'deleteTask',
      'updateTask',
      'updateTaskStatus',
    ]);
    getTasksSpy = apiServiceStub.getTasks.and.returnValue(
      of([
        {
          id: 1,
          title: 'some title 1',
          description: 'some description 1',
          status: TaskStatus.OPEN,
          userId: 1,
        },
        {
          id: 2,
          title: 'some title 2',
          description: 'some description 2',
          status: TaskStatus.DONE,
          userId: 2,
        },
      ])
    );

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        NgPrimeModule,
      ],
      declarations: [TasksComponent, HeaderComponent],
      providers: [{ provide: ApiService, useValue: apiServiceStub }],
    }).compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(TasksComponent);
    component = fixture.componentInstance;
    router.navigate(['tasks']);
    fixture.detectChanges();
  });

  afterEach(() => {
    localStorage.removeItem('token');
  });

  it('should load tasks when mounting component', () => {
    expect(location.path()).toBe('/tasks');
    expect(getTasksSpy).toHaveBeenCalledTimes(1);
  });

  it('should delete a task when clicking delete icon', () => {
    spyOn(component, 'onRowDelete');
    const deleteButton = fixture.debugElement.query(
      By.css('#task-delete-button-1')
    );
    deleteButton.triggerEventHandler('click', null);
    expect(component.onRowDelete).toHaveBeenCalledWith(1);
  });

  it('should cancel updating a task when clicking cancel icon', () => {
    spyOn(component, 'onRowEditInit');
    spyOn(component, 'onRowEditCancel');
    const editButton = fixture.debugElement.query(
      By.css('#task-edit-button-1')
    );
    editButton.triggerEventHandler('click', null);
    expect(component.onRowEditInit).toHaveBeenCalled();

    fixture.detectChanges();

    const cancelButton = fixture.debugElement.query(
      By.css('#task-cancel-edit-button-1')
    );
    cancelButton.triggerEventHandler('click', null);
    expect(component.onRowEditCancel).toHaveBeenCalled();
  });

  it('should save an edited task title and status when clicking save icon', () => {
    spyOn(component, 'onRowEditSave');
    const editButton = fixture.debugElement.query(
      By.css('#task-edit-button-1')
    );
    editButton.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.editedTask).toEqual({
      id: 1,
      title: 'some title 1',
      description: 'some description 1',
      status: TaskStatus.OPEN,
      userId: 1,
    });

    const hostElement = fixture.nativeElement;
    const titleInput: HTMLInputElement = hostElement.querySelector(
      '#task-title-1'
    );
    titleInput.value = 'updated title 1';
    titleInput.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    const dropdown: Dropdown = fixture.debugElement.query(By.css('p-dropdown'))
      .componentInstance;

    dropdown.selectItem('change', {
      label: TaskStatus.IN_PROGRESS,
      value: TaskStatus.IN_PROGRESS,
    });

    dropdown.onChange.emit({ value: TaskStatus.IN_PROGRESS });
    fixture.detectChanges();

    const saveButton = fixture.debugElement.query(
      By.css('#task-confirm-edit-button-1')
    );
    saveButton.triggerEventHandler('click', null);
    expect(component.onRowEditSave).toHaveBeenCalledWith({
      id: 1,
      title: 'updated title 1',
      description: 'some description 1',
      status: TaskStatus.IN_PROGRESS,
      userId: 1,
    });
  });
});
